require("dotenv").config();
const express = require("express");
const router = express.Router();
const wechat = require("wechat");
const sleep = require("util").promisify(setTimeout);
const { firebase } = require("../firebase/admin");
const { sendEmail } = require("../utils/mailHelper");
const {
  broadcastMessage,
  getThumbnailUrl,
  getMonashDetails,
  getUserProfile,
  updateReplyFlag,
} = require("../firebase/firebaseHelper");
const {
  uploadVideo,
  uploadMedia,
  sendText,
  sendVideo,
  sendImage,
} = require("../utils/msgHelper");
const {
  getConversationId,
  updateWatermark,
  getAttachment,
  removeFile,
} = require("../utils/fileHelper");
const {
  sendActivity,
  sendImageActivity,
  getActivityResponse,
  reconnectConversation,
} = require("../utils/directlineHelper");

// Initialize the WeChat-Api.
const config = {
  token: process.env.WeChatToken,
  appid: process.env.WeChatAppId,
  appsecret: process.env.WeChatAppSecret,
  checkSignature: false,
};

const WELCOME_MESSAGE =
  "Ahoy! I'm the peer feedback bot. I’ll be sending you some simple tasks to complete over the next few days, but firstly... what should I call you?";
const INTAKE = process.env.INTAKE;
const settingRef = firebase.doc(`${INTAKE}/WeChatSetting`);

// Register real-time listener for the setting modification in Firebase.
settingRef.onSnapshot(
  async (docSnapshot) => {
    if (!docSnapshot.data()) return;

    var previousSetting = docSnapshot.data().Previous_Setting;
    const uploadFlag = docSnapshot.data().Upload;
    const rankingFlag = docSnapshot.data().Ranking;
    const reallocateFlag = docSnapshot.data().Reallocate;
    const reviewFlag = docSnapshot.data().Review;
    const summaryFlag = docSnapshot.data().Summary;

    // Update the previous setting & send broadcast messages accordingly.
    if (uploadFlag && uploadFlag !== previousSetting.Upload) {
      previousSetting.Upload = uploadFlag;
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Upload");
    }
    if (rankingFlag && rankingFlag !== previousSetting.Ranking) {
      previousSetting.Ranking = rankingFlag;
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Ranking");
    }
    if (reallocateFlag && reallocateFlag !== previousSetting.Reallocate) {
      previousSetting.Reallocate = reallocateFlag;
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Reallocate");
    }
    if (reviewFlag && reviewFlag !== previousSetting.Review) {
      previousSetting.Review = reviewFlag;
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Review");
    }
    if (summaryFlag && summaryFlag !== previousSetting.Summary) {
      previousSetting.Summary = summaryFlag;
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Summary");
    }
  },
  (err) => {
    console.log(`Encountered Error: ${err}`);
  }
);

router.use(express.query());

router.post("/notify/review", async (req, res) => {
  res.sendStatus(200);
  res.end();

  console.log("Review Request Body: ", req.body);
  const message = req.body.message;
  const userId = req.body.userId;

  // Request body verification.
  if (!userId || !message) {
    return console.log("Paramter Invalid...");
  }

  // Try to send the customer service message, if it fails, then send the
  // message through the user's email.
  try {
    let retry = 0;
    while (retry <= 3) {
      const succeed = await trySendText(userId, message);
      if (succeed) break;
      else retry++;
      console.log("Send Text Retry Count: ", retry);
    }
  } catch (error) {
    console.error(error);
    await sendEmail(userId, message);
  }
});

router.post("/notify", async (req, res) => {
  res.sendStatus(200);
  res.end();

  console.log("Notification Request Body: ", req.body);
  const channel = req.body.channel;
  const userId = req.body.userId;
  const message = req.body.message;
  const videoName = req.body.videoName;
  const flag = req.body.flag;
  const type = req.body.type;

  if (!channel || !userId || !message) {
    return console.log("Paramter invalid...");
  }

  let retry = 0;
  while (retry <= 10) {
    const succeed = await trySendText(userId, message);
    if (succeed) break;
    else retry++;
    console.log("Send Text Retry Count: ", retry);
  }

  if (!flag) {
    if (videoName) {
      try {
        const imageUrl = await getThumbnailUrl(videoName);
        const localFilename = await getAttachment(
          imageUrl,
          userId,
          "image/jpg"
        );

        // Upload the local image to WeChat server as a temporary media resource.
        let uploadRetry = 0;
        let results = false;
        while (uploadRetry <= 10) {
          results = await tryUploadMedia(localFilename, "image");
          if (results) break;
          else uploadRetry++;
          console.log("Upload Image Retry Count: ", uploadRetry);
        }
        // Send the image by getting the media id returned.
        const mediaId = results[0].media_id;

        let retry = 0;
        while (retry <= 10) {
          const succeed = await trySendImage(userId, mediaId);
          if (succeed) break;
          else retry++;
          console.log("Send Image Retry Count: ", retry);
        }

        // Set replay flag after sending the attachment.
        await updateReplyFlag(userId);
        // Remove the local video file.
        await removeFile(localFilename);
      } catch (error) {
        console.log(error);
      }
    } else {
      // If no video name attached, then send a message back to the bot.
      if (!(type && type === "idle")) {
        const { conversationId } = await getConversationId(userId);
        await sendActivity(userId, "No", conversationId);
      }
    }
  }
});

router.use(
  "/",
  wechat(config, async function(req, res, next) {
    /*
     * When the response is longer than 5 second, WeChat server will push a same message to the endpoint
     * and up to 3 messages might be received. In order to avoid receiving same messages and forwarding
     * to the bot framework, please send an empty message back to the WeChat server to acknowledge that
     * the message has been received first.
     */
    res.reply("");

    // Retrieve the from user OpenID of the official account and the message.
    const message = req.weixin;
    const messageType = message.MsgType;
    const fromUser = message.FromUserName;
    const userMessage = message.Content;

    if (messageType === "event" && message.Event === "subscribe") {
      let retry = 0;
      while (retry <= 10) {
        const succeed = await trySendText(fromUser, WELCOME_MESSAGE);
        if (succeed) break;
        else retry++;
        console.log("Send Text Retry Count: ", retry);
      }
      return;
    }

    // Create or retrieve the conversation for the current user.
    let { conversationId, watermark } = await getConversationId(fromUser);

    let response;
    // Send text message through the direct line service.
    if (messageType === "text") {
      response = await sendActivity(fromUser, userMessage, conversationId);
    }

    // Send message with image attachment through the direct line service.
    else if (messageType === "image") {
      const contentUrl = message.PicUrl;
      response = await sendImageActivity(fromUser, contentUrl, conversationId);
    }

    // For all other messages types, return error message for now (e.g. video, unsubscribe, audio).
    else {
      if (
        !(
          messageType === "event" &&
          (message.Event === "subscribe" || message.Event === "unsubscribe")
        )
      ) {
        let retry = 0;
        while (retry <= 10) {
          const succeed = await trySendText(
            fromUser,
            "Sorry, I can only understand your text messages or images..."
          );
          if (succeed) break;
          else retry++;
          console.log("Send Text Retry Count: ", retry);
        }
      }
      return;
    }

    // Retrieve the status and watermark of the response.
    const status = response.status;
    watermark = response.data.id;
    await updateWatermark(fromUser, watermark);
    let resMessage = "";

    // 401 - Invalid or missing authroization header.
    // 403 - Toekn or secret is invalid to perform the operation.
    if (status === 403 || status === 401) {
      // Reconnet the conversation and send activity again to the Bot.
      await reconnectConversation(conversationId);
      const retryRes = await sendActivity(
        fromUser,
        userMessage,
        conversationId
      );

      if (retryRes.status !== 200)
        resMessage =
          "Service is temporarily unavailable, please try again later... 😴";
    }

    // 404 - The requested resource was not found.
    // 500 - An internal server error has occurred.
    // 502 - The bot is unavailable or returned an error.
    if (status === 404 || status === 500 || status === 502)
      resMessage =
        "Service is temporarily unavailable, please try again later... 😴";

    // 429 - Too many requests have been submitted.
    if (status === 429)
      resMessage =
        "I have received too many requests and I could not handle it, please try again later... 😴";

    // If error response message exists, send back to the user.
    if (resMessage.length !== 0) {
      try {
        let retry = 0;
        while (retry <= 10) {
          const succeed = await trySendText(fromUser, resMessage);
          if (succeed) break;
          else retry++;
          console.log("Send Text Retry Count: ", retry);
        }
        return;
      } catch (error) {
        console.error(error);
      }
    }

    // Retrieve bot response if 200 OK.
    const { responses, size, index } = await getBotResponse(
      conversationId,
      watermark
    );
    await sendBotResponse(responses, size, index, fromUser);

    // Verify the reply flag, if still false, then video/image is not sent successfully.
    const { id, role } = await getMonashDetails(fromUser);
    if (id && role) {
      const { replyFlag } = await getUserProfile(id, role);
      if (!replyFlag) {
        // console.log("Send Attachment Failed, Retry...");
        const { responses, size, index } = await getBotResponse(
          conversationId,
          watermark
        );
        await sendBotResponse(responses, size, index, fromUser);
      }
    }
  })
);

/**
 * Get bot responses with retry wrapper (in case message being delayed).
 * @param {*} conversationId
 * @param {*} watermark
 */
async function getBotResponse(conversationId, watermark) {
  let responses;
  let size;
  let index = 1;

  let responseRetry = 0;
  while (responseRetry <= 5) {
    await sleep(2000);
    responses = await getActivityResponse(conversationId, watermark);
    const activities = responses.data.activities;

    // Get the last index of the bot replied message.
    size = responses.data.activities.length;

    while (index <= size) {
      if (activities[size - index].from.id === "PeerFeedback") index++;
      else break;
    }

    if (index !== 1 || (size !== 0 && activities[size - 1].text === "No"))
      break;
    else {
      responseRetry++;
      index = 1;
    }
    // console.log("Get Response Error, Retry:", responseRetry);
  }
  return { responses: responses, size: size, index: index };
}

/**
 * Send all the bot responses back to the wechat user.
 * @param {*} responses
 * @param {*} size
 * @param {*} fromUser
 */
async function sendBotResponse(responses, size, index, fromUser) {
  const msgIndex = size - index + 1;
  for (let j = msgIndex; j < size; j++) {
    const activity = responses.data.activities[j];

    // If the response is text message, call the sendText function.
    if (activity.text && activity.text.length !== 0) {
      let retry = 0;
      while (retry <= 10) {
        const succeed = await trySendText(fromUser, activity.text);
        if (succeed) break;
        else retry++;
        console.log("Send Text Retry Count: ", retry);
      }
    }

    if (activity.attachments)
      await sendAttachment(activity.attachments[0], fromUser);
  }
}

/**
 * If the response contains attachment, upload the attachment stream and call
 * the sendVideo/sendImage function.
 * @param {*} attachment
 * @param {*} fromUser
 */
async function sendAttachment(attachment, fromUser) {
  try {
    // Download the attachment based on the content Url.
    const contentUrl = attachment.contentUrl;
    const contentType = attachment.contentType;
    const localFilename = await getAttachment(
      contentUrl,
      fromUser,
      contentType
    );

    if (contentType === "video/mp4") {
      // Upload the local video to WeChat server as a temporary media resource.
      let uploadRetry = 0;
      let results = false;
      while (uploadRetry <= 10) {
        results = await tryUploadVideo(localFilename);
        if (results) break;
        else uploadRetry++;
        console.log("Upload Video Retry Count: ", uploadRetry);
      }

      // Send the video by getting the media id returned.
      const mediaId = results[0].media_id;

      let retry = 0;
      while (retry <= 10) {
        const succeed = await trySendVideo(fromUser, mediaId, null);
        if (succeed) break;
        else retry++;
        console.log("Send Video Retry Count: ", retry);
      }
    }

    if (contentType === "image/jpeg") {
      // Upload the local image to WeChat server as a temporary media resource.
      let uploadRetry = 0;
      let results = false;
      while (uploadRetry <= 10) {
        results = await tryUploadMedia(localFilename, "image");
        if (results) break;
        else uploadRetry++;
        console.log("Upload Image Retry Count: ", uploadRetry);
      }
      // Send the image by getting the media id returned.
      const mediaId = results[0].media_id;

      let retry = 0;
      while (retry <= 10) {
        const succeed = await trySendImage(fromUser, mediaId);
        if (succeed) break;
        else retry++;
        console.log("Send Image Retry Count: ", retry);
      }
    }

    // Set replay flag after sending the attachment and remove local file.
    await updateReplyFlag(fromUser);
    await removeFile(localFilename);
  } catch (error) {
    console.error("Send Attachment Error: ", error);
    await updateReplyFlag(fromUser);
  }
}

/**
 * Try-catch wrapper for wechat-api send text.
 * @param {*} fromUser
 * @param {*} text
 */
async function trySendText(fromUser, text) {
  try {
    await sendText(fromUser, text);
    return true;
  } catch (error) {
    console.log("Send Text Error: ", error);
    return false;
  }
}

/**
 * Try-catch wrapper for wechat-api send video.
 * @param {*} fromUser
 * @param {*} mediaId
 * @param {*} thumbMediaId
 */
async function trySendVideo(fromUser, mediaId, thumbMediaId) {
  try {
    await sendVideo(fromUser, mediaId, thumbMediaId);
    return true;
  } catch (error) {
    console.log("Send Video Error: ", error);
    return false;
  }
}

/**
 * Try-catch wrapper for wechat-api send image.
 * @param {*} fromUser
 * @param {*} mediaId
 */
async function trySendImage(fromUser, mediaId) {
  try {
    await sendImage(fromUser, mediaId);
    return true;
  } catch (error) {
    console.log("Send Image Error: ", error);
    return false;
  }
}

/**
 * Try-catch wrapper for wechat-api upload video.
 * @param {*} localFilename
 */
async function tryUploadVideo(localFilename) {
  try {
    const results = await uploadVideo(localFilename);
    return results;
  } catch (error) {
    console.log("Upload Video Error: ", error);
    return false;
  }
}

/**
 * Try-catch wrapper for wechat-api upload media.
 * @param {*} localFilename
 * @param {*} fileType
 */
async function tryUploadMedia(localFilename, fileType) {
  try {
    const results = await uploadMedia(localFilename, fileType);
    return results;
  } catch (error) {
    console.log("Upload Media Error: ", error);
    return false;
  }
}

module.exports = router;
