const util = require("util");
const WechatAPI = require("wechat-api");

// Initialize the WeChat-Api.
// For production environment, remove the string here.
const config = {
  token: process.env.WeChatToken,
  appid: process.env.WeChatAppId,
  appsecret: process.env.WeChatAppSecret,
  checkSignature: false,
};

const api = new WechatAPI(config.appid, config.appsecret);

// Promisify the api.getMedia function.
const getMedia = util.promisify((mediaId, callback) =>
  api.getMedia(mediaId, (err, ...results) => callback(err, results))
);
// Promisify the api.uploadVideo function.
const uploadVideo = util.promisify((filepath, callback) =>
  api.uploadVideo(filepath, (err, ...results) => callback(err, results))
);
// Promisify the api.uploadImage function.
const uploadMedia = util.promisify((filepath, type, callback) =>
  api.uploadMedia(filepath, type, (err, ...results) => callback(err, results))
);
// Promisify the api.sendText function.
const sendText = util.promisify((openId, message, callback) =>
  api.sendText(openId, message, (err, ...results) => callback(err, results))
);
// Promisify the api.sendVideo function.
const sendVideo = util.promisify((openId, mediaId, thumbMediaId, callback) =>
  api.sendVideo(openId, mediaId, thumbMediaId, (err, ...results) =>
    callback(err, results)
  )
);
// Promisify the api.sendImage function.
const sendImage = util.promisify((openId, mediaId, callback) =>
  api.sendImage(openId, mediaId, (err, ...results) => callback(err, results))
);
// Promisify the api.sendTemplate function.
const sendTemplate = util.promisify((openId, templateId, url, data, callback) =>
  api.sendTemplate(openId, templateId, url, data, (err, ...results) =>
    callback(err, results)
  )
);
// Promisify the api.massSendText function.
const massSendText = util.promisify((content, receivers, callback) =>
  api.massSendText(content, receivers, (err, ...results) =>
    callback(err, results)
  )
);

exports.getMedia = getMedia;
exports.uploadVideo = uploadVideo;
exports.uploadMedia = uploadMedia;
exports.sendText = sendText;
exports.sendVideo = sendVideo;
exports.sendImage = sendImage;
exports.sendTemplate = sendTemplate;
exports.massSendText = massSendText;
