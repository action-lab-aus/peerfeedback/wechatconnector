const axios = require("axios");

// For production environment, remove the string here.
const SECRET = process.env.DirectLineSecret;
const DIRECTLINE = "https://directline.botframework.com/v3/directline";

/**
 * Start the conversation and return the current conversation id.
 */
async function startConversation() {
  try {
    const response = await axios({
      method: "POST",
      headers: { Authorization: `Bearer ${SECRET}` },
      url: `${DIRECTLINE}/conversations`,
    });
    return response.data.conversationId;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Send activity to the bot framework conversation.
 * @param {String} fromUser
 * @param {String} message
 * @param {String} conversationId
 */
async function sendActivity(fromUser, message, conversationId) {
  try {
    return await axios({
      method: "POST",
      headers: {
        Authorization: `Bearer ${SECRET}`,
        "Content-Type": "application/json",
      },
      url: `${DIRECTLINE}/conversations/${conversationId}/activities`,
      data: {
        type: "message",
        from: { name: "WeChat", id: fromUser },
        text: message,
      },
    });
  } catch (error) {
    console.error(error);
    return error.response;
  }
}

/**
 * Send activity with image attachments to the bot framework conversation.
 * @param {String} fromUser
 * @param {String} contentUrl
 * @param {String} conversationId
 */
async function sendImageActivity(fromUser, contentUrl, conversationId) {
  try {
    return await axios({
      method: "POST",
      headers: {
        Authorization: `Bearer ${SECRET}`,
        "Content-Type": "application/json",
      },
      url: `${DIRECTLINE}/conversations/${conversationId}/activities`,
      data: {
        type: "message",
        from: { name: "WeChat", id: fromUser },
        attachments: [
          {
            contentType: "image/jpeg",
            contentUrl: contentUrl,
          },
        ],
      },
    });
  } catch (error) {
    console.error(error);
    return error.response;
  }
}

/**
 * Send activity with video attachments to the bot framework conversation.
 * @param {String} fromUser
 * @param {String} videoUrl
 * @param {String} conversationId
 */
async function sendVideoActivity(fromUser, videoUrl, conversationId) {
  try {
    return await axios({
      method: "POST",
      headers: {
        Authorization: `Bearer ${SECRET}`,
        "Content-Type": "application/json",
      },
      url: `${DIRECTLINE}/conversations/${conversationId}/activities`,
      data: {
        type: "message",
        from: { name: "WeChat", id: fromUser },
        attachments: [
          {
            contentType: "video/mp4",
            contentUrl: videoUrl,
          },
        ],
      },
    });
  } catch (error) {
    console.error(error);
    return error.response;
  }
}

/**
 * Retrieve responses for the current bot framework conversation.
 * @param {String} conversationId
 * @param {String} watermark
 */
async function getActivityResponse(conversationId, watermark) {
  try {
    const watermarkParam = watermark
      ? `?watermark=${watermark.split("|")[1]}`
      : "";
    const response = await axios({
      method: "GET",
      headers: { Authorization: `Bearer ${SECRET}` },
      url: `${DIRECTLINE}/conversations/${conversationId}/activities${watermarkParam}`,
    });
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Reconnect the conversation if the session is expired.
 * @param {String} conversationId
 */
async function reconnectConversation(conversationId) {
  try {
    return await axios({
      method: "GET",
      headers: { Authorization: `Bearer ${SECRET}` },
      url: `${DIRECTLINE}/conversations/${conversationId}`,
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * End the conversation if all necessary data has been gathered.
 * @param {String} fromUser
 * @param {String} conversationId
 */
async function endConversation(fromUser, conversationId) {
  try {
    return await axios({
      method: "POST",
      headers: {
        Authorization: `Bearer ${SECRET}`,
        "Content-Type": "application/json",
      },
      url: `${DIRECTLINE}/conversations/${conversationId}/activities`,
      data: {
        type: "endOfConversation",
        from: {
          id: fromUser,
        },
      },
    });
  } catch (error) {
    console.error(error);
  }
}

exports.startConversation = startConversation;
exports.sendActivity = sendActivity;
exports.sendImageActivity = sendImageActivity;
exports.sendVideoActivity = sendVideoActivity;
exports.getActivityResponse = getActivityResponse;
exports.reconnectConversation = reconnectConversation;
exports.endConversation = endConversation;
