const fs = require("fs");
const util = require("util");
const axios = require("axios");
const { startConversation } = require("./directlineHelper");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const BOT_TOKEN = "./bot-token.json";

/**
 * Get the conversation id and return to the connector.
 * If conversation already exists, retrieve from the .json file.
 * Otherwise, generate the id by starting a new conversation.
 * @param {String} fromUser
 */
async function getConversationId(fromUser) {
  let allUserToken = await getAllUserToken();

  if (!allUserToken.hasOwnProperty(fromUser)) {
    conversationId = await startConversation();
    allUserToken[fromUser] = { conversation: conversationId };
    await recordUserToken(allUserToken);
    return { conversationId: conversationId, watermark: null };
  } else {
    return {
      conversationId: allUserToken[fromUser]["conversation"],
      watermark: allUserToken[fromUser]["watermark"],
    };
  }
}

/**
 * Remove the user token details when conversation ended.
 * @param {String} fromUser
 */
async function removeUserToken(fromUser) {
  let allUserToken = await getAllUserToken();
  delete allUserToken[fromUser];
  await recordUserToken(allUserToken);
}

/**
 * Read all user tokens from the .json file.
 */
async function getAllUserToken() {
  try {
    const jsonString = await readFile(BOT_TOKEN, "utf8");
    return JSON.parse(jsonString);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Update the user token information and write to the .json file.
 * @param {Array} allUserToken
 */
async function recordUserToken(allUserToken) {
  try {
    const jsonString = JSON.stringify(allUserToken);
    await writeFile(BOT_TOKEN, jsonString);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Update the current watermark of the conversation to keep track of the
 * conversation status.
 * @param {String} fromUser
 * @param {String} watermark
 */
async function updateWatermark(fromUser, watermark) {
  try {
    let allUserToken = await getAllUserToken();
    allUserToken[fromUser]["watermark"] = watermark;
    await recordUserToken(allUserToken);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Download a specific attachment and store it in the local directory.
 * @param {String} url
 * @param {String} openId
 * @param {String} contentType
 */
async function getAttachment(url, openId, contentType) {
  // Local file path for the bot to save the attachment.
  const localFileName = `./${openId}.${contentType.split("/")[1]}`;

  try {
    const response = await axios.get(url, { responseType: "arraybuffer" });

    // If user uploads JSON file, this prevents it from being written as
    // "{"type":"Buffer","data":[123,13,10,32,32,34,108...".
    if (response.headers["content-type"] === "application/json") {
      response.data = JSON.parse(response.data, (key, value) => {
        return value && value.type === "Buffer"
          ? Buffer.from(value.data)
          : value;
      });
    }

    // Write the file to the local directory.
    await writeFile(localFileName, response.data, (fsError) => {
      if (fsError) throw fsError;
    });
  } catch (error) {
    console.error(error);
    return undefined;
  }

  // If no error, then return the localFileName.
  return localFileName;
}

/**
 * Remove a specific file from the local directory.
 * @param {String} localFileName
 */
async function removeFile(localFileName) {
  try {
    fs.unlinkSync(localFileName);
  } catch (error) {
    console.error(error);
  }
}

exports.getConversationId = getConversationId;
exports.removeUserToken = removeUserToken;
exports.updateWatermark = updateWatermark;
exports.getAttachment = getAttachment;
exports.removeFile = removeFile;
