require("dotenv").config();
const admin = require("firebase-admin");
const serviceAccount = JSON.parse(process.env.PeerFeedbackAdmin);

// Initialize the Firebase.
try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.FirestoreUrl,
    storageBucket: process.env.FirebaseStorageUrl,
  });
} catch (error) {
  console.error("Failed to connected to Firebase...");
}

const db = admin.firestore();
const bucket = admin.storage().bucket();

exports.firebase = db;
exports.admin = admin;
exports.bucket = bucket;
