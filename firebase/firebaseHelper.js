// Initialize the Firebase references.
const { firebase, bucket } = require("./admin");
const { massSendText } = require("../utils/msgHelper");

const INTAKE = process.env.INTAKE;
const channelRef = firebase.doc(`${INTAKE}/Channel`);
const userProfileRef = firebase.collection(`${INTAKE}/User/UserProfile`);
const tutorRef = firebase.collection(`${INTAKE}/User/TutorProfile`);

// Define the broadcast messages.
const BROADCAST_MESSAGE = {
  Upload:
    "Please submit a link to your draft video. 1. We can only process public Google Drive, Spark or OneDrive Personal links. 2. Videos must be less than 100MB. I will take some time to verify the video link, so if I don't reply immediately, don't panic!",
  Ranking:
    "Everyone has now submitted a video. Next, I will ask you to rank (order) different groups of videos. In each group, I will show you three videos (A,B,C) which you need to rank in order for a few criteria. /:strong when you want the first video.",
  Reallocate:
    "We still have videos which require feedback. Send /:strong if you are up for doing more ranking!",
  Review:
    "Rankings for your video are ready. /:strong when you want to see them.",
  Summary:
    "A recipient of your ranking would like a little more information about how you made your decision. /:strong when you have some time to go into more detail.",
};

/**
 * Send broadcast messages to all registered students.
 * @param {String} stage
 */
async function broadcastMessage(stage) {
  return console.log("Broadcast Stage: ", stage);
  // try {
  //   const snapshot = await channelRef.collection("DIRECTLINE").get();

  //   let openIdList = [];
  //   snapshot.forEach((document) => {
  //     if (document.data().Monash_Id) openIdList.push(document.id);
  //   });

  //   let retry = 0;
  //   while (retry <= 10) {
  //     const succeed = await tryMassSendText(stage, openIdList);
  //     if (succeed) break;
  //     else retry++;
  //     console.log("Send Broadcast Message Retry Count: ", retry);
  //   }
  // } catch (error) {
  //   console.error(error);
  // }
}

/**
 * Get a signed Url of the video thumbnail from Firebase.
 * @param {String} videoOwner The current video owner's student id.
 */
async function getThumbnailUrl(videoOwner) {
  try {
    const config = {
      action: "read",
      expires: "03-17-2025",
    };

    const file = bucket.file(
      `${INTAKE}/Thumbnails/${videoOwner.split(".")[0]}.jpg`
    );
    const data = await file.getSignedUrl(config);
    return data[0];
  } catch (error) {
    console.error(error);
    return undefined;
  }
}

/**
 * Get the current student id from the channel.
 * @param {String} fromUser
 */
async function getMonashDetails(fromUser) {
  try {
    const snapshot = await channelRef
      .collection("DIRECTLINE")
      .doc(fromUser)
      .get();
    return { id: snapshot.data().Monash_Id, role: snapshot.data().Role };
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the user profile from Firestore.
 * @param {String} id Current user's monash id number.
 * @param {String} role Current user's role.
 */
async function getUserProfile(id, role) {
  try {
    let snapshot;
    if (role === "Student") snapshot = await userProfileRef.doc(id).get();
    else snapshot = await tutorRef.doc(id).get();

    return {
      index: snapshot.data().Index,
      name: snapshot.data().User_Name,
      channel: snapshot.data().Channel,
      userId: snapshot.data().User_Id,
      videoUrl: snapshot.data().Video_Url,
      replyFlag: snapshot.data().Reply_Flag,
      audienceFlag: snapshot.data().Audience_Flag,
      visualFlag: snapshot.data().Visual_Flag,
      knowledgeFlag: snapshot.data().Knowledge_Flag,
      providedNo: snapshot.data().Rank_Provided_No,
      receivedNo: snapshot.data().Rank_Received_No,
      email: snapshot.data().Email,
    };
  } catch (error) {
    console.error(error);
  }
}

/**
 * // Set replay flag after sending the video/image.
 * @param {*} fromUser
 */
async function updateReplyFlag(fromUser) {
  const { id, role } = await getMonashDetails(fromUser);
  if (role === "Student")
    await userProfileRef.doc(id).update({ Reply_Flag: true });
  else await tutorRef.doc(id).update({ Reply_Flag: true });
}

/**
 * Try-catch wrapper for wechat-api send broadcast messages.
 * @param {*} stage
 * @param {*} openIdList
 */
async function tryMassSendText(stage, openIdList) {
  try {
    await massSendText(BROADCAST_MESSAGE[stage], openIdList);
    return true;
  } catch (error) {
    return false;
  }
}

exports.broadcastMessage = broadcastMessage;
exports.getMonashDetails = getMonashDetails;
exports.getUserProfile = getUserProfile;
exports.getThumbnailUrl = getThumbnailUrl;
exports.updateReplyFlag = updateReplyFlag;
