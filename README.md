# WeChatConnector

WeChatConnector has been created using [Direct Line Service](https://docs.microsoft.com/en-us/azure/bot-service/rest-api/bot-framework-rest-direct-line-3-0-concepts?view=azure-bot-service-4.0), which:

- Connects to the Microsoft Bot Framework for handling the Waterfall Dialog
- Enables WeChat Official Account as a social media channel for the Bot Framework
- Handles broadcasting messages to the WeChat users
- Sending customized Customer Service Messages to a specific WeChat user

## Prerequisites

### 1. [Node.js](https://nodejs.org) version 10.14 or higher

```bash
# determine node version
node --version
```

### 2. [WeChat Official Account](https://mp.weixin.qq.com/)

- A verified WeChat Subscription Account is required
- Record your WeChat Token, App ID and App Secret for setting up the environment variables

### 3. [Firebase Project](https://console.firebase.google.com/u/0/)

- Create a Firebase project in the online console
- Download Firebase Admin Service Account
  - In the Firebase console, open Settings > Service Accounts
  - Click Generate New Private Key, then confirm by clicking Generate Key
  - Securely store the JSON file containing the key
  - Update your Service Account under `./firebase/admin.js`
- Enable Firestore and Storage
- Record the Firestore Url and Storage Url for setting the environment variables

### 4. [Nodemailer with Gmail](https://nodemailer.com/about/)

Bot initiated messages can only be sent to the user within the 24-hour window for WeChat. So if the user has not interacted with the bot for more than 24 hours, the bot cannot send messages (e.g. notification, broadcasting, etc.) to the user. In that case, we need to use email as the backup channel for sending those messages.

#### 4.1 Setup Google Cloud Platform

- Go to Google Cloud and create a project
- Search for APIs & Services
- Click on `Credentials > Create Credentials > OAuth Client ID`

  - Type: Web Application
  - Name: Name_of_Application
  - Authorized Redirect URIs: https://developers.google.com/oauthplayground

- Copy both the Client ID and Client Secret

#### 4.2 Get Refresh Token from Google OAuth Playground

- Go to Oauth Playground, and choose `Setting > Enable use your own OAuth credentials > Enter OAuth Client ID & OAuth Client Secret > Close`
- Under Select & Authorize APIs, Type `https://mail.google.com > Authorize APIs`, and log in with the account that you want to send the email from
- Click exchange authorization code to get the Refresh Token

#### 4.3 Set Environment Variables for the Mailing Service

In the `.env` file, set the following variables:

```
SENDER_EMAIL_ADDRESS
MAILING_SERVICE_CLIENT_ID
MAILING_SERVICE_CLIENT_SECRET
MAILING_SERVICE_REFRESH_TOKEN
```

# Deploy the Bot to Azure

After creating the connector, you can deploy it to Azure to make it accessible from anywhere.
To learn how, see [Deploy your bot to Azure](https://aka.ms/azuredeployment) for a complete set of deployment instructions.
